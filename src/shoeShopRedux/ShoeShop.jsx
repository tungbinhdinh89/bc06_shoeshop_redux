import React, { Component } from 'react';
import CartShoe from './CartShoe';
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';

export default class extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-6">
            <CartShoe />
          </div>
          <div className="col-6">
            <ListShoe />
          </div>
        </div>
        <DetailShoe />
      </div>
    );
  }
}
