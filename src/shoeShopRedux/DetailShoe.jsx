import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GET_DETAIL } from './redux/constant/constantShoe';

class DetailShoe extends Component {
  render() {
    let { name, price, description, shortDescription, quantity, image } =
      this.props.detailShoe;
    return (
      <div>
        <div className="card">
          <img className="card-img-top" style={{ width: 200 }} src={image} />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{quantity}</li>
            <li className="list-group-item">{description}</li>
            <li className="list-group-item">{shortDescription}</li>
          </ul>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoe: state.shoeShopReducer.detailShoe,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (shoe) => {
      let action = {
        type: GET_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailShoe);
