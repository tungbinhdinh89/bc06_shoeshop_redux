import { shoeArr } from '../../datashoe';
import {
  CHANGE_QUANTITY,
  GET_DETAIL,
  ADD_SHOE,
  DELETE_SHOE,
} from '../constant/constantShoe';
let initialState = {
  shoeArr: shoeArr,
  detailShoe: shoeArr[0],
  cart: [],
};
export const shoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DETAIL: {
      //   state.detailShoe = action.payload;
      return { ...state, detailShoe: action.payload };
    }
    case ADD_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let newShoe = { ...action.payload, quantityCart: 1 };
        cloneCart.push(newShoe);
        console.log('cloneCart: ', cloneCart);
      } else {
        cloneCart[index].quantityCart++;

        console.log('cloneCart qunatity: ', cloneCart[0].quantityCart);
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      cloneCart[index].quantityCart =
        cloneCart[index].quantityCart + action.payload.option;

      cloneCart[index].quantityCart == 0 && cloneCart.splice(index, 1);

      return { ...state, cart: cloneCart };
    }
    case DELETE_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return (item.id = action.payload);
      });
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
