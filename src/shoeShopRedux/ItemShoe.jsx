import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ADD_SHOE, GET_DETAIL } from './redux/constant/constantShoe';

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left">
          <img
            style={{ width: '10vw' }}
            className="card-img-top"
            src={this.props.dataShoe.image}
          />
          <div className="card-body text-center">
            <h4 className="card-title">{this.props.dataShoe.price}</h4>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.dataShoe);
              }}
              className="btn btn-warning">
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddProduct(this.props.dataShoe);
              }}
              className="btn btn-success">
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddProduct: (shoe) => {
      let action = {
        type: ADD_SHOE,
        payload: shoe,
      };
      dispatch(action);
    },
    handleChangeDetail: (shoe) => {
      let action = {
        type: GET_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
