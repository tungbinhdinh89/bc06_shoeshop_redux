import ShoeShop from '../shoeShopRedux/ShoeShop';
import './App.css';

function App() {
  return (
    <div className="App">
      <ShoeShop />
    </div>
  );
}

export default App;
